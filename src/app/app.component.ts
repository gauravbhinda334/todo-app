import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'project-one';
  display='none';
  public taskName;
  public taskDesc;
  public finalTask;
  parentFunction(data)
  {
    console.warn(data);
    this.display=data;
  }

  taskAddFunction(data){
    this.taskName=data.email;
    this.taskDesc=data.body;
    this.finalTask={
      email:this.taskName,
      body:this.taskDesc
    }
  };
}
