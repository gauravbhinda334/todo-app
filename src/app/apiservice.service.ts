import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

    private theUrl="https://jsonplaceholder.typicode.com/posts/1/comments";
    constructor(private http:HttpClient){

    }

    getData(){
        return this.http.get(this.theUrl);
    }
}
