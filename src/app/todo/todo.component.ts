import { Component, Input, Output, SimpleChanges } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDragEnter, CdkDragExit, CdkDragStart, CdkDrag } from '@angular/cdk/drag-drop';

import { ApiserviceService } from '../apiservice.service';
import { ShareService } from '../share.service';


@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent {

  @Input() pushTask;

  public key=0;
  public allTasks=[
    [1,"Trial task 1","Todo tasks are important for staying organized and on track. Prioritize important tasks, make a schedule, and break down large tasks into smaller one"],
    [2,"Trial task 2"," Stay focused and avoid distractions. Reward yourself for completing tasks. Keep a positive attitude and stay motivated."]
  ];

  constructor(private service:ApiserviceService,private ss: ShareService){

  };

  ngOnInit(){

  }

  ngOnChanges(changes: SimpleChanges,): void {

   
    this.key+=1;
    this.allTasks.push([this.key,this.pushTask.email,this.pushTask.body]);
    console.log(this.allTasks);

  };

  deleteTask(keyID)
  {
      for(var i=0;i<this.allTasks.length;i++)
      {
        if(this.allTasks[i][0]===keyID)
        {
          this.allTasks.splice(i,1);
        }
      }
      console.log(this.allTasks);
  }


  onDrop(event: CdkDragDrop<any>) {
    this.ss.drop(event);
  }

  }

