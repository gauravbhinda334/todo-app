import { Component, Input, Output, SimpleChanges } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDragEnter, CdkDragExit, CdkDragStart, CdkDrag } from '@angular/cdk/drag-drop';

import { ApiserviceService } from '../apiservice.service';
import { ShareService } from '../share.service';


@Component({
  selector: 'app-onhold',
  templateUrl: './onhold.component.html',
  styleUrls: ['./onhold.component.css']
})
export class OnholdComponent {

  @Input() pushTask;

  public key=0;
  public allTasks=[[-1,"Random","Functionality Test bla bla bla"]];

  constructor(private service:ApiserviceService,private ss: ShareService){

  };

  ngOnInit(){

  }

  ngOnChanges(changes: SimpleChanges,): void {

    this.key+=1;
    this.allTasks.push([this.key,this.pushTask.email,this.pushTask.body]);
    console.log(this.allTasks);

  };

  deleteTask(keyID)
  {
    for(var i=0;i<this.allTasks.length;i++)
    {
      if(this.allTasks[i][0]===keyID)
      {
        this.allTasks.splice(i,1);
      }
    }
    console.log(this.allTasks);
  }


  onDrop(event: CdkDragDrop<any>) {
    this.ss.drop(event);
  }

  }

