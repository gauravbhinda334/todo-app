import { Component , OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent {

  @Output() parentFunction:EventEmitter<any>=new EventEmitter();
  constructor(){

  };

  ngOnInit(){

  };

  opencard(){
    this.parentFunction.emit("flex");
  }
}
