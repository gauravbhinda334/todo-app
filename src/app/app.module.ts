import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { TodoComponent } from './todo/todo.component';
import { InprogressComponent } from './inprogress/inprogress.component';
import { OnholdComponent } from './onhold/onhold.component';
import { CompletedComponent } from './completed/completed.component';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import { AddtaskComponent } from './addtask/addtask.component';
import {MatIconModule} from '@angular/material/icon'
import {HttpClientModule} from '@angular/common/http';
import { ApiserviceService } from './apiservice.service';
import { DragDropModule} from '@angular/cdk/drag-drop';
import { ShareService } from './share.service';




@NgModule({
  declarations: [
    AppComponent,
    TopNavComponent,
    TodoComponent,
    InprogressComponent,
    OnholdComponent,
    CompletedComponent,
    AddtaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatDividerModule,
    MatCardModule,
    MatIconModule,
    HttpClientModule,
    DragDropModule
  ],
  providers: [ApiserviceService, ShareService],
  bootstrap: [AppComponent]
})
export class AppModule { }
