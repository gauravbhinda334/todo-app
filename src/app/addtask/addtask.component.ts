import { Component,OnInit,Input, OnChanges, SimpleChanges,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-addtask',
  templateUrl: './addtask.component.html',
  styleUrls: ['./addtask.component.css']
})
export class AddtaskComponent implements OnChanges {

  @Input() display;
  @Output() parentFunction:EventEmitter<any>=new EventEmitter();
  @Output() taskAddFunction:EventEmitter<any>=new EventEmitter();

  public todo_task="";
  public todo_desc="";

  constructor(){

  }
  
  ngOnChanges(changes: SimpleChanges): void {

    console.log(this.display);
    document.getElementById('container-alpha').style.display=this.display;

  }

  ngOnInit():void{

  }

  addtask()
  {
    this.todo_task = (<HTMLInputElement>document.getElementById("task")).value;

    this.todo_desc = (<HTMLInputElement>document.getElementById("desc")).value;

    this.taskAddFunction.emit({
      email:this.todo_task,
      body:this.todo_desc
    });
    
    this.closetask();

    
  }

  closetask()
  {
    (<HTMLInputElement>document.getElementById("task")).value="";
    (<HTMLInputElement>document.getElementById("desc")).value="";

    this.parentFunction.emit("none");
  }

}
